CFLAGS=-Wall -Wextra -Wno-unused -O3 -std=gnu99 -ggdb3
PREFIX:=/usr/local

all: compctld compctl

compctld: cgroup.o compctld.o
compctl: cgroup.o compctl.o

compctl.o:: compctl.c help-in-quotes

help-in-quotes: README.client
	cat $< | sed -e '0,/^Usage/d; /^Description/,$$d' | \
		tail -n +3 | sed -e 's/\(.*\)/"\1\\n"/' >$@

install:
	install -g root -m 755 -o root -t $(DESTDIR)$(PREFIX)/bin compctl
	install -g root -m 755 -o root -t $(DESTDIR)$(PREFIX)/sbin compctld
	install -g root -m 755 -o root -t $(DESTDIR)$(PREFIX)/share/man/man1 compctl.1

clean:
	rm -f compctl compctld compctl.o compctld.o cgroup.o help-in-quotes
