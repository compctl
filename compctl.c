#define _GNU_SOURCE /* struct ucred */
#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include "cgroup.h"
#include "common.h"


bool unlimited;


char *
daemon_chat(char *cmd)
{
	int s = socket(AF_UNIX, SOCK_STREAM, 0);
	struct sockaddr_un sun = { .sun_family = AF_UNIX, .sun_path = SOCKFILE };
	if (connect(s, (struct sockaddr *) &sun, sizeof(sun.sun_family) + strlen(sun.sun_path) + 1) < 0) {
		perror(SOCKFILE);
		fputs("Plese contact " ADMINCONTACT " about this error.\n", stderr);
		exit(EXIT_FAILURE);
	}

	/* Send command. */

	struct iovec iov_cmd = {
		.iov_base = cmd,
		.iov_len = strlen(cmd),
	};
	struct msghdr msg = {
		.msg_iov = &iov_cmd,
		.msg_iovlen = 1,
	};

	/* Include credentials in the message. */
	struct ucred cred = {
		.pid = getpid(),
		.uid = getuid(),
		.gid = getgid(),
	};
	char cbuf[CMSG_SPACE(sizeof(cred))];
	msg.msg_control = cbuf;
	msg.msg_controllen = sizeof(cbuf);
	struct cmsghdr *cmsg = CMSG_FIRSTHDR(&msg);
	cmsg->cmsg_level = SOL_SOCKET;
	cmsg->cmsg_type = SCM_CREDENTIALS;
	cmsg->cmsg_len = CMSG_LEN(sizeof(cred));
	memcpy(CMSG_DATA(cmsg), &cred, sizeof(cred));

	ssize_t sent = sendmsg(s, &msg, 0);
	if (sent < 0) {
		perror("sendmsg");
		exit(EXIT_FAILURE);
	}
	if ((size_t) sent < msg.msg_iov->iov_len) {
		fprintf(stderr, "incomplete send %zd < %zu, FIXME\n", sent, msg.msg_iov->iov_len);
		exit(EXIT_FAILURE);
	}

	/* Receive reply. */

	char reply[1024];
	struct iovec iov_reply = {
		.iov_base = reply,
		.iov_len = sizeof(reply),
	};
	msg.msg_iov = &iov_reply;
	msg.msg_iovlen = 1;

recvagain:;
	int replylen = recvmsg(s, &msg, 0);
	if (replylen < 0) {
		if (errno == EAGAIN)
			goto recvagain;
		perror("recvmsg");
		exit(EXIT_FAILURE);
	}
	if (replylen >= 1024) {
		fprintf(stderr, "too long reply from the server\n");
		exit(EXIT_FAILURE);
	}
	reply[replylen] = 0;

	return strdup(reply);
}


void
help(FILE *f)
{
	fputs("compctl - Computations under control\n\n"
#include "help-in-quotes"
	"Contact " ADMINCONTACT " with bug reports and comments.\n", f);
}

int
run(int argc, char *argv[])
{
	if (!unlimited) {
		char *line = daemon_chat("blessme");
		if (line[0] != '1') {
			fprintf(stderr, "%s\n", *line ? line : "unexpected hangup");
			return EXIT_FAILURE;
		}
		free(line);
	}

	setpgrp();
	if (setpriority(PRIO_PROCESS, 0, COMPNICE) < 0)
		perror("Warning: setpriority()");

	char *argvx[argc + 1];
	for (int i = 0; i < argc; i++)
		argvx[i] = argv[i];
	argvx[argc] = NULL;
	execvp(argvx[0], argvx);
	perror("execvp");
	return EXIT_FAILURE;
}

int
screen(int argc, char *argv[])
{
	char *argvx[argc + 2];
	argvx[0] = "screen";
	argvx[1] = "-m";
	for (int i = 0; i < argc; i++)
		argvx[i + 2] = argv[i];
	return run(argc + 2, argvx);
}

void
kill_task(pid_t pid)
{
	if (unlimited) {
		fputs("Killing computations not possible when computations are not limited..\n", stderr);
		exit(EXIT_FAILURE);
	}

	char cmd[256]; snprintf(cmd, sizeof(cmd), "kill %d", pid);
	char *line = daemon_chat(cmd);
	if (line[0] != '1') {
		fprintf(stderr, "%s\n", *line ? line : "unexpected hangup");
		exit(EXIT_FAILURE);
	}
	free(line);
}

void
kill_all(void)
{
	if (unlimited) {
		fputs("Killing computations not possible when computations are not limited..\n", stderr);
		exit(EXIT_FAILURE);
	}

	char *line = daemon_chat("killall");
	if (line[0] != '1') {
		fprintf(stderr, "%s\n", *line ? line : "unexpected hangup");
		exit(EXIT_FAILURE);
	}
	puts(line + 2);
	free(line);
}

void
limit_mem(size_t limit)
{
	if (unlimited) {
		fputs("Computations are not limited.\n", stderr);
		exit(EXIT_FAILURE);
	}

	char cmd[256];
	snprintf(cmd, sizeof(cmd), "limitmem %zu", limit * 1048576);
	char *line = daemon_chat(cmd);
	if (line[0] != '1') {
		/* TODO: More error message postprocessing. */
		fprintf(stderr, "%s\n", *line ? line : "unexpected hangup");
		if (line[0] == '0') {
			fprintf(stderr, "Most likely, the computations are already using too much memory.\n"
					"Consider killing some of them first.\n");
		}
		exit(EXIT_FAILURE);
	}
	free(line);
}

void
usage(void)
{
	if (unlimited) {
		puts("unlimited");
		return;
	}

	size_t usage = cgroup_get_mem_usage(chier, cgroup);
	size_t limit = cgroup_get_mem_limit(chier, cgroup);
	printf("Memory usage:\t%zuM / %zuM\n", usage / 1048576, limit / 1048576);
}

void
list(void)
{
	if (unlimited) {
		fputs("List not available when computations are not limited.\n", stderr);
		exit(EXIT_FAILURE);
	}

	pid_t *tasks;
	int tasks_n = cgroup_task_list(chier, cgroup, &tasks);
	if (tasks_n < 0)
		exit(EXIT_FAILURE);
	for (int i = 0; i < tasks_n; i++) {
		/* TODO: Print process details. */
		printf("%d\n", tasks[i]);
	}
}

int
main(int argc, char *argv[])
{
	int optind = 1;

	if (argc == optind) {
		help(stderr);
		return EXIT_FAILURE;
	}

	unlimited = access("/etc/compctl-unlimited", F_OK) >= 0;
	if (!unlimited && !cgroup_available(chier, cgroup)) {
		fputs("CGroup-based computation control is not available on this host.\n", stderr);
		fputs("Most likely, this computer is not meant for computations,\n", stderr);
		fputs("please consider switching to a different host.\n", stderr);
		fputs("Plese contact " ADMINCONTACT " if you believe this is an error.\n", stderr);
		return EXIT_FAILURE;
	}

	while (argc > optind) {
		char *cmd = argv[optind++];
		if (!strcmp(cmd, "--run")) {
			if (argc <= optind) {
				fputs("missing arguments for --run\n", stderr);
				exit(EXIT_FAILURE);
			}
			return run(argc - optind, &argv[optind]);

		} else if (!strcmp(cmd, "--screen")) {
			if (argc <= optind) {
				fputs("missing arguments for --screen\n", stderr);
				exit(EXIT_FAILURE);
			}
			return screen(argc - optind, &argv[optind]);

		} else if (!strcmp(cmd, "--usage")) {
			usage();

		} else if (!strcmp(cmd, "--list")) {
			list();

		} else if (!strcmp(cmd, "--kill")) {
			if (argc <= optind) {
				fputs("missing argument for --kill\n", stderr);
				exit(EXIT_FAILURE);
			}
			kill_task(atoi(argv[optind++]));

		} else if (!strcmp(cmd, "--killall")) {
			kill_all();

		} else if (!strcmp(cmd, "--limitmem")) {
			if (argc <= optind) {
				fputs("missing argument for --limitmem\n", stderr);
				exit(EXIT_FAILURE);
			}
			limit_mem(atol(argv[optind++]));

		} else if (!strcmp(cmd, "--help")) {
			help(stdout);

		} else {
			help(stderr);
			return EXIT_FAILURE;
		}
	}

	return EXIT_SUCCESS;
}
