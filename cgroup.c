#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <unistd.h>

#include "cgroup.h"
#include "common.h"

#if SWAP_LIMIT
#define MEMSW ".memsw"
#else
#define MEMSW
#endif

void (*cgroup_perror)(const char *s) = perror;
static const char cgroupfs[] = "/sys/fs/cgroup/";

int
cgroup_setup(const char *chier, const char *controllers)
{
	char chierdir[PATH_MAX];
	snprintf(chierdir, sizeof(chierdir), "%s%s", cgroupfs, chier);
	if (access(chierdir, F_OK) >= 0)
		return 0;

	if (mount("none", cgroupfs, "tmpfs", 0, NULL) < 0) {
		cgroup_perror(cgroupfs);
		return -1;
	}
	if (mkdir(chierdir, 0777) < 0) {
		cgroup_perror(chierdir);
		return -1;
	}
	if (mount("none", chierdir, "cgroup", 0, controllers) < 0) {
		cgroup_perror(chierdir);
		return -1;
	}
	return 1;
}

int
cgroup_create(const char *chier, const char *cgroup)
{
	char cgroupdir[PATH_MAX];
	snprintf(cgroupdir, sizeof(cgroupdir), "%s%s/%s", cgroupfs, chier, cgroup);
	if (access(cgroupdir, F_OK) >= 0)
		return 0;
	if (mkdir(cgroupdir, 0777) < 0) {
		cgroup_perror(cgroupdir);
		return -1;
	}
	return 1;
}

bool
cgroup_available(const char *chier, const char *cgroup)
{
	char cgroupdir[PATH_MAX];
	snprintf(cgroupdir, sizeof(cgroupdir), "%s%s/%s", cgroupfs, chier, cgroup);
	return (access(cgroupdir, F_OK) >= 0);
}


int
cgroup_add_task(const char *chier, const char *cgroup, pid_t pid)
{
	char tasksfile[PATH_MAX];
	snprintf(tasksfile, sizeof(tasksfile), "%s%s/%s/tasks", cgroupfs, chier, cgroup);
	FILE *tasks = fopen(tasksfile, "a");
	if (!tasks) {
		cgroup_perror(tasksfile);
		return -1;
	}
	if (fprintf(tasks, "%d\n", pid) < 0) {
		cgroup_perror(tasksfile);
		fclose(tasks);
		return -1;
	}
	if (fclose(tasks) < 0) {
		cgroup_perror(tasksfile);
		return -1;
	}
	return 1;
}

int
cgroup_is_task_in_cgroup(const char *chier, const char *cgroup, pid_t pid)
{
	char tasksfile[PATH_MAX];
	snprintf(tasksfile, sizeof(tasksfile), "%s%s/%s/tasks", cgroupfs, chier, cgroup);
	FILE *tasks = fopen(tasksfile, "r");
	if (!tasks) {
		cgroup_perror(tasksfile);
		return -1;
	}

	bool exists = false;
	char line[128];
	while (fgets(line, sizeof(line), tasks)) {
		if (atoi(line) == pid) {
			exists = true;
			break;
		}
	}
	fclose(tasks);
	return exists;
}

int
cgroup_task_list(const char *chier, const char *cgroup, pid_t **tasklist)
{
	char tasksfile[PATH_MAX];
	snprintf(tasksfile, sizeof(tasksfile), "%s%s/%s/tasks", cgroupfs, chier, cgroup);
	FILE *tasks = fopen(tasksfile, "r");
	if (!tasks) {
		cgroup_perror(tasksfile);
		return -1;
	}

	int ntasks = 0;
	*tasklist = NULL;
	char line[128];
	while (fgets(line, sizeof(line), tasks)) {
		if (!(ntasks % 32))
			*tasklist = realloc(*tasklist, (ntasks + 32) * sizeof(*tasklist));
		(*tasklist)[ntasks++] = atoi(line);
	}
	fclose(tasks);
	return ntasks;
}


size_t
cgroup_get_mem_limit(const char *chier, const char *cgroup)
{
	char limitfile[PATH_MAX];
	snprintf(limitfile, sizeof(limitfile), "%s%s/%s/memory"MEMSW".limit_in_bytes", cgroupfs, chier, cgroup);
	FILE *limit = fopen(limitfile, "r");
	if (!limit) {
		cgroup_perror(limitfile);
		return -1;
	}
	size_t nlimit = 0;
	fscanf(limit, "%zu", &nlimit);
	fclose(limit);
	return nlimit;
}

static int
cgroup_set_mem_limit_do(const char *chier, const char *cgroup, size_t nlimit, char *memsw)
{
	char limitfile[PATH_MAX];
	snprintf(limitfile, sizeof(limitfile), "%s%s/%s/memory%s.limit_in_bytes", cgroupfs, chier, cgroup, memsw);
	FILE *limit = fopen(limitfile, "w");
	if (!limit) {
		cgroup_perror(limitfile);
		return -1;
	}
	if (fprintf(limit, "%zu\n", nlimit) < 0) {
		cgroup_perror(limitfile);
		fclose(limit);
		return -1;
	}
	if (fclose(limit) < 0) {
		cgroup_perror(limitfile);
		return -1;
	}
	return 1;
}

static int
cgroup_set_mem_limit_twice(const char *chier, const char *cgroup, size_t nlimit, char *memsw1, char *memsw2)
{
	int ret = cgroup_set_mem_limit_do(chier, cgroup, nlimit, memsw1);
	if (ret >= 0) {
		int ret2 = cgroup_set_mem_limit_do(chier, cgroup, nlimit, memsw2);
		ret = ret2 < 0 ? ret2 : (ret || ret2);
	}
	return ret;
}

int
cgroup_set_mem_limit(const char *chier, const char *cgroup, size_t nlimit)
{
#if SWAP_LIMIT
	/* We need to set both the "normal" and memsw limits, but in such
	 * order that normal <= memsw always holds. */
	size_t curlimit = cgroup_get_mem_limit(chier, cgroup);
	if (nlimit < curlimit)
		return cgroup_set_mem_limit_twice(chier, cgroup, nlimit, "", MEMSW);
	else
		return cgroup_set_mem_limit_twice(chier, cgroup, nlimit, MEMSW, "");
#else
	return cgroup_set_mem_limit_do(chier, cgroup, nlimit, "");
#endif
}

size_t
cgroup_get_mem_usage(const char *chier, const char *cgroup)
{
	char usagefile[PATH_MAX];
	snprintf(usagefile, sizeof(usagefile), "%s%s/%s/memory"MEMSW".usage_in_bytes", cgroupfs, chier, cgroup);
	FILE *usage = fopen(usagefile, "r");
	if (!usage) {
		cgroup_perror(usagefile);
		return -1;
	}
	size_t nusage = 0;
	fscanf(usage, "%zu", &nusage);
	fclose(usage);
	return nusage;
}
