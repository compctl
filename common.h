/* Definitions common for the client and daemon,
 * and runtime configuration definitions. */

#ifndef COMMON__H
#define COMMON__H

/* Compile-time configuration area. */

/* Contact of the network administrators. */
#define ADMINCONTACT "<wizards@kam.mff.cuni.cz>"

/* Default memory split policy. Memory is split between
 * user and computations by split_ratio, but so that neither
 * has reserved less than minfree (in case of too little memory,
 * user takes precedence) and user does not have reserved more
 * than maxfree. */
#define static_minfree (512*1048576UL)
#define static_maxfree (2048*1048576UL)
#define split_ratio 0.5

/* Default nice value for processes started using compctl --run. */
#define COMPNICE 12

/* Comment out if swap memory resource accounting is disabled
 * and only RAM memory should be limited. */
#define SWAP_LIMIT 1


/* Other common definitions. */
/* See README for the high-level protocol description. */

#define SOCKFILE "/var/run/compctl.sock"

/* CGroup and hierarchy names. */
static const char chier[] = "memory";
static const char cgroup[] = "comp";

#endif
