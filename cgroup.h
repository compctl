/* Tiny cgroup manipulation toolkit. libcgroup is a mess. */

#ifndef CGROUP__H
#define CGROUP__H

#include <stdbool.h>
#include <sys/types.h>

/* In case of errors, this function is called; it is expected to
 * report @s and errno contents. By default, it is bound to perror,
 * but you can change that if you want e.g. to syslog errors. */
extern void (*cgroup_perror)(const char *s);

/* Setup the cgroups machinery and @chier cgroups hierarchy using
 * @controllers. Return 0 if cgroups already set up, 1 on success,
 * -1 on failure. */
int cgroup_setup(const char *chier, const char *controllers);
/* Setup a given control group. Return 0 if cgroups already set up,
 * 1 on success, -1 on failure. */
int cgroup_create(const char *chier, const char *cgroup);
/* Return true if cgroup control is set up, false otherwise. */
bool cgroup_available(const char *chier, const char *cgroup);

/* Add a task to a given cgroup. Return 1 on success, -1 on failure. */
int cgroup_add_task(const char *chier, const char *cgroup, pid_t pid);
/* Check if a task is in a given cgroup. Return 0/1, -1 on failure. */
int cgroup_is_task_in_cgroup(const char *chier, const char *cgroup, pid_t pid);
/* Store a list of tasks in a given cgroup to @tasks. Returns number of tasks
 * or -1 on failure. */
int cgroup_task_list(const char *chier, const char *cgroup, pid_t **tasks);

/* Get memory limit of a given cgroup. Return (size_t) -1 on failure. */
size_t cgroup_get_mem_limit(const char *chier, const char *cgroup);
/* Set a memory limit of a given cgroup. Return 1 on success, -1 on failure. */
int cgroup_set_mem_limit(const char *chier, const char *cgroup, size_t limit);
/* Get memory usage of a given cgroup. Return (size_t) -1 on failure. */
size_t cgroup_get_mem_usage(const char *chier, const char *cgroup);

#endif
